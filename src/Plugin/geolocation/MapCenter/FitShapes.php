<?php

namespace Drupal\rocketship_location\Plugin\geolocation\MapCenter;

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\geolocation\MapCenterInterface;
use Drupal\geolocation\MapCenterBase;

/**
 * Fixed coordinates map center.
 *
 * @MapCenter(
 *   id = "fit_shapes",
 *   name = @Translation("Fit shapes"),
 *   description = @Translation("Automatically fit map to displayed locations based on shapes (no markers available)."),
 * )
 */
class FitShapes extends MapCenterBase implements MapCenterInterface {

  /**
   * {@inheritdoc}
   */
  public function alterMap(array $map, $center_option_id, array $center_option_settings, $context = NULL) {
    $map = parent::alterMap($map, $center_option_id, $center_option_settings, $context);
    $map['#attached'] = BubbleableMetadata::mergeAttachments($map['#attached'], [
      'library' => [
        'rocketship_location/map_center.fitshapes',
      ],
    ]);

    return $map;
  }

}
