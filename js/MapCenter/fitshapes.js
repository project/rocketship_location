/**
 * @file
 * Fit shapes.
 */

(function (Drupal) {

  'use strict';

  Drupal.geolocation = Drupal.geolocation || {};
  Drupal.geolocation.mapCenter = Drupal.geolocation.mapCenter || {};

  /**
   * @param {GeolocationMapInterface} map
   */
  Drupal.geolocation.mapCenter.fit_shapes = function (map) {
    if (typeof map.mapMarkers === 'undefined') {
      return false;
    }

    if (map.mapMarkers.length === 0) {
      const coordinates = map.loadShapesFromContainer()[0].coordinates;
      const lastIndex = coordinates.length - 1;

      map.leafletMap.fitBounds([
        [coordinates[0].lat, coordinates[0].lng],
        [coordinates[lastIndex].lat, coordinates[lastIndex].lng]
      ]);

      return true;
    }

    return false;
  }

})(Drupal);
